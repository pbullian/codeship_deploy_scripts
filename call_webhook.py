import requests
import sys
import os
headers = {'X-Access-Token': os.environ['ACCESS_TOKEN']}

r = requests.get(os.environ['WH_URL']+'/'+sys.argv[1]+'/'+sys.argv[2]+'/null', headers=headers)

if (r.status_code == 200 and "ERROR" not in r.text and "Failed" not in r.text):
    print('succesfully called webhook')
    sys.exit(0)
else:
    print(r.text)
    sys.exit(1)
